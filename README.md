# X3D plugin

This plugin provides support for importing and exporting graph object to the X3D format.

It includes support of the file types : 
-'.x3d' (for both import and export)
-'.wrl' (for export)
-'.x3dv' (for export)
