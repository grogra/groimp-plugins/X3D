package de.grogra.ext.x3d;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;

import org.apache.xmlbeans.XmlException;
import de.grogra.ext.x3d.importation.SceneImport;
import de.grogra.ext.x3d.xmlbeans.X3DDocument;
import de.grogra.ext.x3d.xmlbeans.SceneDocument.Scene;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.ui.Workbench;

public class X3DImport extends FilterBase implements ObjectSource {

	/**
	 * Current import instance.
	 */
	protected static X3DImport theImport = null;

	/**
	 * Stack of parsers.
	 */
	protected Stack<X3DParser> parsers = null;

	/**
	 * GroIMP root node of imported x3d nodes.
	 */
	protected Null sceneNode = null;

	/**
	 * Current workbench.
	 */
	protected Workbench workbench = null;

	public X3DImport(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(IOFlavor.valueOf(Node.class));
	}

	//	/**
	//	 * Constructor of the x3d importer. Makes some preprocessing tasks.
	//	 * @param item
	//	 * @param source
	//	 */
	//	public X3DImport(FilterItem item, FilterSource source) {
	//		super(item, source);
	//		setFlavor(IOFlavor.valueOf(Node.class));
	//
	//		
	//		this.workbench = Workbench.current();
	//		
	//		workbench.beginStatus(this);
	//		workbench.setStatus(this, "Import X3D", -1);
	//		
	//		
	//		theImport = this;
	//		parsers = new Stack<X3DParser>();
	//		
	////		File file = (File)((FileSource)source).getFile();
	//		//File file = new File(source.getSystemId());
	//				
	//		X3DDocument x3dDocument = null;
	//		try (InputStream is = ((FileSource)source).getInputStream();){
	//			try {
	//				x3dDocument = (X3DDocument) X3DDocument.Factory.parse(is);
	//			} catch (XmlException e) {e.printStackTrace();return;}
	//			  catch (IOException e) {e.printStackTrace();return;}
	//
	//		} catch (IOException e) {
	//			e.printStackTrace();
	//		}
	//		
	//		
	//		Scene scene = x3dDocument.getX3D().getScene();
	//		sceneNode = SceneImport.createInstance(scene);
	//		
	//		X3DParser sceneParser = new X3DParser(scene, (FileSource) source);
	//		addNewParser(sceneParser);
	//		sceneParser.parseScene(sceneNode);
	//		removeCurrentParser();
	//		
	//		workbench.clearStatusAndProgress(this);
	//	}

	public Object getObject() throws IOException {

		this.workbench = Workbench.current();

		workbench.beginStatus(this);
		workbench.setStatus(this, "Import X3D", -1);


		theImport = this;
		parsers = new Stack<X3DParser>();

		//		File file = (File)((FileSource)source).getFile();
		//File file = new File(source.getSystemId());

		X3DDocument x3dDocument = null;
		try (InputStream is = ((FileSource)source).getInputStream();){
			try {
				x3dDocument = (X3DDocument) X3DDocument.Factory.parse(is);
			} catch (XmlException e) {e.printStackTrace();return null;}
			catch (IOException e) {e.printStackTrace();return null;}

		} catch (IOException e) {
			e.printStackTrace();
		}


		Scene scene = x3dDocument.getX3D().getScene();
		sceneNode = SceneImport.createInstance(scene);

		X3DParser sceneParser = new X3DParser(scene, (FileSource) source);
		addNewParser(sceneParser);
		sceneParser.parseScene(sceneNode);
		removeCurrentParser();

		workbench.clearStatusAndProgress(this);

		return sceneNode;
	}

	/**
	 * Returns the instance of the current x3d importer.
	 * @return
	 */
	public static X3DImport getTheImport() {
		return theImport;
	}

	/**
	 * Returns the current x3d parser.
	 * @return
	 */
	public X3DParser getCurrentParser() {
		return parsers.peek();
	}

	/**
	 * Add a new parser on top of stack. This is the current parser.
	 * @param parser
	 */
	public void addNewParser(X3DParser parser) {
		parsers.push(parser);
	}

	/**
	 * Removes the current parser from stack. Next one in
	 * stack becomes the current parser.
	 */
	public void removeCurrentParser() {
		parsers.pop();
	}

	public void increaseProgress() {
		workbench.setStatus(this, "Import X3D");
		workbench.setIndeterminateProgress(this);
	}

}
