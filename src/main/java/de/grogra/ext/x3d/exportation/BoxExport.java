package de.grogra.ext.x3d.exportation;

import java.io.IOException;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.CoordinateDocument.Coordinate;
import de.grogra.ext.x3d.xmlbeans.IndexedFaceSetDocument.IndexedFaceSet;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TextureCoordinateDocument.TextureCoordinate;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public class BoxExport extends BaseExport implements AxisExport {

	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode, Transform transformNode)
			throws IOException {
		// length, width, height
		float width = (float) node.getDouble (de.grogra.imp.objects.Attributes.WIDTH);
		float length = (float) node.getDouble (de.grogra.imp.objects.Attributes.LENGTH);
		float height = (float) node.getDouble (de.grogra.imp.objects.Attributes.HEIGHT);

		// export box as ifs to get correct uv's
		IndexedFaceSet ifs = shapeNode.addNewIndexedFaceSet();
		
		// coordinates
		Coordinate coord = ifs.addNewCoordinate();
		coord.setPoint(
				-width/2 + " " + -length/2 + " " +   height/2 + ", " +
				 width/2 + " " + -length/2 + " " +   height/2 + ", " +
				 width/2 + " " +  length/2 + " " +   height/2 + ", " +
				-width/2 + " " +  length/2 + " " +   height/2 + ", " +

				-width/2 + " " + -length/2 + " " +  -height/2 + ", " +
				 width/2 + " " + -length/2 + " " +  -height/2 + ", " +
				 width/2 + " " +  length/2 + " " +  -height/2 + ", " +
				-width/2 + " " +  length/2 + " " +  -height/2 + ", "
		);
	
		// texture coordinates
		TextureCoordinate texCoord = ifs.addNewTextureCoordinate();
		texCoord.setPoint(
				"0.25 0 " +
				"0.5  0 " +
				"0.5  0.3333 " +
				"0.25 0.3333 " +
				
				"0.75 0.3333 " +
				"0.75 0.6666 " +
				"0.5  0.6666 " +
				"0.5  0.3333 " +
				
				"0.5  1 " +
				"0.25 1 " +
				"0.25 0.6666 " +
				"0.5  0.6666 " +
				
				"0    0.6666 " +
				"0    0.3333 " +
				"0.25 0.3333 " +
				"0.25 0.6666 " +
				
				"0.25 0.3333 " +
				"0.5  0.3333 " +
				"0.5  0.6666 " +
				"0.25 0.6666 " +
				
				"1    0.6666 " +
				"0.75 0.6666 " +
				"0.75 0.3333 " +
				"1    0.3333"
		);
		
		// indices
		ifs.setCoordIndex(
				"0 1 2 3 -1 " +
				"1 5 6 2 -1 " +
				"5 4 7 6 -1 " +
				"4 0 3 7 -1 " +
				"3 2 6 7 -1 " +
				"4 5 1 0"
		);
		
		ifs.setTexCoordIndex(
				" 0  1  2  3 -1 " +
				" 4  5  6  7 -1 " +
				" 8  9 10 11 -1 " +
				"12 13 14 15 -1 " +
				"16 17 18 19 -1 " +
				"20 21 22 23"
		);
	}

}
