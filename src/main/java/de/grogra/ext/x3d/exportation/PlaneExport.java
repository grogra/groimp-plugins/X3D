package de.grogra.ext.x3d.exportation;

import java.io.IOException;
import javax.vecmath.Point3d;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.CoordinateDocument.Coordinate;
import de.grogra.ext.x3d.xmlbeans.IndexedFaceSetDocument.IndexedFaceSet;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public class PlaneExport extends BaseExport {

	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode,
			Transform transformNode) throws IOException {
		IndexedFaceSet ifs = shapeNode.addNewIndexedFaceSet();

		ifs.setCoordIndex("0 1 2 3");
		ifs.setSolid(false);
		
		Coordinate coord = ifs.addNewCoordinate();
		
		Point3d p1 = new Point3d(-50, 0, -50);
		Point3d p2 = new Point3d(-50, 0, 50);
		Point3d p3 = new Point3d(50, 0, 50);
		Point3d p4 = new Point3d(50, 0, -50);
		
		coord.setPoint( p1.x + " " + p1.y + " " + p1.z
				+ " " + p2.x + " " + p2.y + " " + p2.z
				+ " " + p3.x + " " + p3.y + " " + p3.z
				+ " " + p4.x + " " + p4.y + " " + p4.z);

	}

}
