package de.grogra.ext.x3d.exportation;

import java.io.IOException;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.CylinderDocument.Cylinder;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public class CylinderExport extends BaseExport implements AxisExport {

	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode,
			Transform transformNode) throws IOException {
		Cylinder c = shapeNode.addNewCylinder();
		
		// length
		float height = (float) node.getDouble (de.grogra.imp.objects.Attributes.LENGTH);
		if (height != 2.0f)
			c.setHeight(height);
		
		// radius
		float radius = node.getFloat (de.grogra.imp.objects.Attributes.RADIUS);
		if (radius != 1.0f)
			c.setRadius(radius);

	}

}
