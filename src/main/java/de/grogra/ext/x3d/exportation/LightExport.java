package de.grogra.ext.x3d.exportation;

import java.io.IOException;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;
import de.grogra.ext.x3d.X3DExport;
import de.grogra.ext.x3d.xmlbeans.SceneDocument.Scene;
import de.grogra.ext.x3d.xmlbeans.ShapeDocument.Shape;
import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.graph.GraphState;
import de.grogra.imp3d.objects.DirectionalLight;
import de.grogra.imp3d.objects.PointLight;
import de.grogra.imp3d.objects.SpotLight;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.math.RGBColor;
import de.grogra.imp3d.objects.GlobalTransformation;

public class LightExport extends BaseExport {

	@Override
	protected void exportImpl(Leaf node, X3DExport export, Shape shapeNode, Transform transformNode)
			throws IOException {
		Object light = node.getObject(de.grogra.imp3d.objects.Attributes.LIGHT);
		
		if (light instanceof SpotLight) {
			handleSpotLight((SpotLight) light, transformNode);
		}
		else if (light instanceof PointLight) {
			handlePointLight((PointLight) light, transformNode);
		}
		else if (light instanceof DirectionalLight) {
			// use scene as parent because directional lights only illuminates
			// children of the parent in x3d
			Scene sceneNode = export.getScene();
			handleDirectionalLight((DirectionalLight) light, node, sceneNode, export.getGraphState());
		}
	}

	private void handleSpotLight(SpotLight light, Transform parent) {
		de.grogra.ext.x3d.xmlbeans.SpotLightDocument.SpotLight slNode = parent.addNewSpotLight();
		
		// color
		RGBColor color = light.getColor();
		if (!color.equals(new RGBColor(1.0f, 1.0f, 1.0f)))
			slNode.setColor(color.x + " " + color.y + " " + color.z);
		
		// direction
		Vector3d dirVec = new Vector3d(0, 0, 1);
		slNode.setDirection(dirVec.x + " " + dirVec.z + " " + -dirVec.y);
		
		// intensity
		float power = light.getPower()/100f;
		if (power != 1.0f)
			slNode.setIntensity(power/100f);
	}
	
	private void handlePointLight(PointLight light, Transform parent) {
		de.grogra.ext.x3d.xmlbeans.PointLightDocument.PointLight plNode = parent.addNewPointLight();
		
		// color
		RGBColor color = light.getColor();
		if (!color.equals(new RGBColor(1.0f, 1.0f, 1.0f)))
			plNode.setColor(color.x + " " + color.y + " " + color.z);
		
		// intensity
		float power = light.getPower() / 100f;
		if (power != 1.0f)
			plNode.setIntensity(power);
	}
	
	private void handleDirectionalLight(DirectionalLight light, Leaf node, Scene parent, GraphState gs) {
		de.grogra.ext.x3d.xmlbeans.DirectionalLightDocument.DirectionalLight dlNode = parent.addNewDirectionalLight();
		
		// color
		RGBColor color = light.getColor();
		if (!color.equals(new RGBColor(1.0f, 1.0f, 1.0f)))
			dlNode.setColor(color.x + " " + color.y + " " + color.z);
		
		// direction
		Matrix4d transMat = GlobalTransformation.get(node.object, true, gs, true).toMatrix4d();
		Vector3d dirVec = new Vector3d(0, 0, 1);
		transMat.transform(dirVec);
		dlNode.setDirection(dirVec.x + " " + dirVec.z + " " + -dirVec.y);
		
		// intensity
		float power = light.getPowerDensity() / 10.0f;
		if (power != 1.0f)
			dlNode.setIntensity(power);
	}
	
}
