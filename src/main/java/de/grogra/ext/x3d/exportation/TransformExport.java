package de.grogra.ext.x3d.exportation;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import de.grogra.ext.x3d.xmlbeans.TransformDocument.Transform;
import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Node;

public class TransformExport {

	public static Transform handleTransform(Matrix4d matrix, Transform parent) {
		return handleTransform(matrix, parent, false, null, null);
	}
	
	public static Transform handleTransform(Matrix4d matrix, Transform parent, boolean isAxis, Node node, GraphState gs) {
		Transform t = parent.addNewTransform();
		
		// translation
		Matrix4f tm = new Matrix4f(matrix);
		Vector3f trans = new Vector3f();
		tm.get(trans);
		if (isAxis) {
			trans.z += gs.getDouble(node, true, de.grogra.imp.objects.Attributes.LENGTH)/2f;
		}
		
		// rotation
		Quat4f rot = new Quat4f();
		tm.get(rot);
		AxisAngle4f arot = new AxisAngle4f();
		arot.set(rot);
		
		// uniform scale factor
		float scale = tm.getScale();
		
		//TODO: scaleOrientation/shear
		
		if (!trans.equals(new Vector3f(0, 0, 0)))
			t.setTranslation(trans.x + " " + trans.z + " " + -trans.y);
		if (arot.angle != 0)
			t.setRotation(arot.x + " " + arot.z + " " + -arot.y + " " + arot.angle);
		if (scale != 1)
			//t.setScale(tm.m00 + " " + tm.m22 + " " + tm.m11);
			t.setScale(scale + " " + scale + " " + scale);
		
		return t;
	}

}
