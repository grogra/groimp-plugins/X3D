package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.CoordinateDocument.Coordinate;

public class CoordinateImport {

	public static float[] createInstance(Coordinate coordinate) {
		float[] c = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (coordinate.isSetUSE()) {
			c = (float[]) referenceMap.get(coordinate.getUSE());
		}
		else {
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (coordinate.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, coordinate);
			}
			
			c = setValues(coordinate, finalValueMap);
			if (coordinate.isSetDEF()) {
				referenceMap.put(coordinate.getDEF(), c);
			}				
		}
	
		return c;
	}
	
	private static float[] setValues(Coordinate coordinate, HashMap<String, String> valueMap) {
		// coordinate
		float[] c;
		String cString = valueMap.containsKey("point") ? valueMap.get("point") : coordinate.getPoint();
		c = Util.splitStringToArrayOfFloat(cString);
		return c;
	}
	
}
