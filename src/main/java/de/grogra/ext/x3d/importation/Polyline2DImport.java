package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.Polyline2DDocument.Polyline2D;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Line;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Phong;
import de.grogra.imp3d.shading.Shader;
import de.grogra.math.ChannelMap;
import de.grogra.math.RGBColor;

public class Polyline2DImport {

	public static Null createInstance(Polyline2D polyline, Node parent) {
		Null n = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (polyline.isSetUSE()) {
			try {
				n = (Null) ((Node) referenceMap.get(polyline.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Null();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (polyline.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, polyline);
			}
			
			setValues(n, polyline, finalValueMap, parent);
			if (polyline.isSetDEF()) {
				referenceMap.put(polyline.getDEF(), n);
			}
		}
		
		n.setName("X3DPolyline2D");
		return n;
	}
	
	private static void setValues(Null node, Polyline2D polyline, HashMap<String, String> valueMap, Node parent) {
		// lineSegments
		float[] lineSegments = null;
		String lineSegmentsString = valueMap.containsKey("lineSegments") ? valueMap.get("lineSegments") : polyline.getLineSegments();
		lineSegments = Util.splitStringToArrayOfFloat(lineSegmentsString, new float[]{});
		
		RGBColor appColor = new RGBColor(0, 0, 0);
		if (parent instanceof ShadedNull) {
			Shader s = ((ShadedNull) parent).getShader();
			if (s instanceof Phong) {
				ChannelMap cm = ((Phong) s).getEmissive();
				if (cm instanceof RGBColor) {
					appColor = (RGBColor) cm; 
				}
			}
		}
		
		// create points, set translations
		int pointCount = lineSegments.length;
		for (int i = 0; i < pointCount - 2; i=i+2) {
			float x = lineSegments[i+0];
			float y = 0;
			float z = lineSegments[i+1];
			float dx = lineSegments[i+2] - lineSegments[i+0];
			float dy = 0;
			float dz = lineSegments[i+3] - lineSegments[i+1];
			Line newLine = new Line(x, y, z, dx, dy, dz);
			newLine.setColor(appColor.x, appColor.y, appColor.z);
			newLine.setName("X3DLine");
			node.addEdgeBitsTo(newLine, Graph.BRANCH_EDGE, null);
		}
	}
	
}
