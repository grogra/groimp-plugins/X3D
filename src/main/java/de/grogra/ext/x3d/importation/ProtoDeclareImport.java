package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.ProtoBodyDocument.ProtoBody;
import de.grogra.ext.x3d.xmlbeans.ProtoDeclareDocument.ProtoDeclare;

public class ProtoDeclareImport {

	protected ProtoBody protoBody = null;
	
	public static void createInstance(ProtoDeclare protoDeclare) {
		ProtoDeclareImport pdi = new ProtoDeclareImport();
	
		HashMap<String, ProtoDeclareImport> protoMap = X3DImport.getTheImport().getCurrentParser().getProtoMap();
		
		ProtoBody protoBody = protoDeclare.getProtoBody();
		pdi.protoBody = protoBody;
		
		protoMap.put(protoDeclare.getName(), pdi);
	}
	
	public ProtoBody getProtoBody() {
		return protoBody;
	}

}
