package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.xmlbeans.BillboardDocument.Billboard;
import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.impl.Node;

public class BillboardImport {

	public static Node createInstance(Billboard billboard) {
		Node n = null;
		
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();

		if (billboard.isSetUSE()) {
			try {
				n = ((Node) referenceMap.get(billboard.getUSE())).cloneGraph(EdgePatternImpl.TREE, true);
			} catch (CloneNotSupportedException e) {e.printStackTrace();}
		}
		else {
			n = new Node();
			if (billboard.isSetDEF()) {
				referenceMap.put(billboard.getDEF(), n);
			}
		}
		n.setName("X3DBillboard");
		return n;		
	}
	
}
