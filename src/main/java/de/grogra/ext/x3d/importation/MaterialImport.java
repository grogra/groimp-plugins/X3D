package de.grogra.ext.x3d.importation;

import java.util.HashMap;

import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.MaterialDocument.Material;
import de.grogra.imp3d.shading.Phong;
import de.grogra.math.Graytone;
import de.grogra.math.RGBColor;

public class MaterialImport {

	public static Phong createInstance(Material mat) {
		Phong p = null;
	
		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (mat.isSetUSE()) {
			p = ((Phong) referenceMap.get(mat.getUSE())).clone();
		}
		else {
			p = new Phong();
			
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (mat.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, mat);
			}
			
			setValues(p, mat, finalValueMap);
			if (mat.isSetDEF()) {
				referenceMap.put(mat.getDEF(), p);
			}
		}
		return p;
	}
	
	private static void setValues(Phong p, Material mat, HashMap<String, String> valueMap) {
		// ambientIntensity
		float ambientIntensity;
		ambientIntensity = (valueMap.get("ambientIntensity") != null) ? Float.valueOf(valueMap.get("ambientIntensity")) : mat.getAmbientIntensity();  
		p.setAmbient(new Graytone(ambientIntensity));

		// diffuseColor
		RGBColor diffuseColor = new RGBColor();
		String diffuseColorString = (valueMap.get("diffuseColor") != null) ? valueMap.get("diffuseColor") : mat.getDiffuseColor();
		Util.splitStringToTuple3f(diffuseColor, diffuseColorString);
		p.setDiffuse(diffuseColor);

		// emissiveColor
		RGBColor emissiveColor = new RGBColor();
		String emissiveColorString = (valueMap.get("emissiveColor") != null) ? valueMap.get("emissiveColor") : mat.getEmissiveColor();
		Util.splitStringToTuple3f(emissiveColor, emissiveColorString);
		p.setEmissive(emissiveColor);
		
		// shininess
		float shininess;
		shininess = (valueMap.get("shininess") != null) ? Float.valueOf(valueMap.get("shininess")) : mat.getShininess();  
		p.setShininess(new Graytone(shininess));
		
		// specularColor
		RGBColor specularColor = new RGBColor();
		String specularColorString = (valueMap.get("specularColor") != null) ? valueMap.get("specularColor") : mat.getSpecularColor();
		Util.splitStringToTuple3f(specularColor, specularColorString);
		p.setSpecular(specularColor);
		
		// transparency
		float transparency;
		transparency = (valueMap.get("transparency") != null) ? Float.valueOf(valueMap.get("transparency")) : mat.getTransparency();  
		p.setTransparency(new Graytone(transparency));
	}
}
