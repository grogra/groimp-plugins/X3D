package de.grogra.ext.x3d.importation;

import java.util.HashMap;
import de.grogra.ext.x3d.ProtoInstanceImport;
import de.grogra.ext.x3d.X3DImport;
import de.grogra.ext.x3d.util.Util;
import de.grogra.ext.x3d.xmlbeans.TextureCoordinateDocument.TextureCoordinate;

public class TextureCoordinateImport {

	public static float[] createInstance(TextureCoordinate textureCoordinate) {
		float[] tc = null;

		HashMap<String, Object> referenceMap = X3DImport.getTheImport().getCurrentParser().getReferenceMap();
		
		if (textureCoordinate.isSetUSE()) {
			tc = (float[]) referenceMap.get(textureCoordinate.getUSE());
		}
		else {
			HashMap<String, String> finalValueMap = new HashMap<String, String>();
			if (textureCoordinate.isSetIS()) {
				ProtoInstanceImport.calcFinalValueMap(finalValueMap, textureCoordinate);
			}
			
			tc = setValues(textureCoordinate, finalValueMap);
			if (textureCoordinate.isSetDEF()) {
				referenceMap.put(textureCoordinate.getDEF(), tc);
			}				
		}
	
		return tc;
	}
	
	private static float[] setValues(TextureCoordinate textureCoordinate, HashMap<String, String> valueMap) {
		// color
		float[] tc;
		String tcString = valueMap.containsKey("point") ? valueMap.get("point") : textureCoordinate.getPoint();
		tc = Util.splitStringToArrayOfFloat(tcString);
		return tc;
	}
	
}
